const path = require('path');
const fs = require('fs');

const config = require('../configs/paths');

const serviceTemplate = require('./templates/service');
const routeTemplate = require('./templates/route');
const responseTemplate = require('./templates/response');
const requestTemplate = require('./templates/request');
const moduleTemplate = require('./templates/module');
const helperTemplate = require('./templates/helper');
const middlewareTemplate = require('./templates/middleware');
const dataFormatTemplate = require('./templates/dataFormat');

const templates = {
    serviceTemplate,
    routeTemplate,
    responseTemplate,
    requestTemplate,
    moduleTemplate,
    helperTemplate,
    middlewareTemplate,
    dataFormatTemplate
}

const getTemplate = (templateName) => {
    return templates[templateName];
}

const setModuleName = (moduleName, template) => {
    moduleName = moduleName.split("/").pop();
    template = template.replace('#moduleName', moduleName);
    return template;
}

const createFile = (name, script, dir) => {
    let filename = `${dir}/${name}.js`;
    fs.writeFileSync(filename, script);
}

const declareModule = (moduleCategory, moduleName) => {
    let modulePath = moduleName;
    moduleName = moduleName.split("/").pop();
    moduleCategory = moduleCategory.charAt(0).toUpperCase() + moduleCategory.slice(1) + "s"; 
    let fullpath = path.join(config.apppath, `${moduleCategory}.js`)
    let add = `export { default as ${moduleName}} from '../${moduleCategory.toLowerCase()}/${modulePath}';\n`;
    let content = ''

    if (fs.existsSync(fullpath)) {
        content = fs.readFileSync(fullpath, 'utf8');
    }
    content = content.replace(add, '');
    content = content + add;

    fs.writeFileSync(fullpath, content);
}

module.exports = (moduleCategory, moduleName) => {
    let template = getTemplate(moduleCategory + "Template");
    let moduleFile = setModuleName(moduleName, template);
    let modulePath = config[`${moduleCategory}Path`];
    createFile(moduleName, moduleFile, modulePath);
    declareModule(moduleCategory, moduleName);

    console.log('------------------------------------');
    console.log(`${moduleName} has been created. `);
    console.log('------------------------------------');
}