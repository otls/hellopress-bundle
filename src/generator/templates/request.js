module.exports = `
import { Validation } from "../app/Modules";

export default class #moduleName extends Validation {

    constructor() {
        super();
    }

    async authorized(req, res, next) {
        return false;
    }

    async file(req, res, next) {
        return false;
    }

    async rules(req, res, next) {
        return {
           
        }
    }
}`;
